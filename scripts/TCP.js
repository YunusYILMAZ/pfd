// create a tcp modbus client
const {
    remote
} = require('electron')
const modbus = require('jsmodbus')
const net = require('net')
const controlModule = require('./scripts/controlModule')
const AP = require('./scripts/autopilot')
const {
    fail
} = require('assert')
const {
    data
} = require('jquery')
const socket = new net.Socket()
const options = {
    'host': '192.168.0.100',
    'port': '502'
}

const client = new modbus.client.TCP(socket)

/* Keybord Values */
var RUN_TIME = 0
var FPM_ = 0;
var roll_ = 0;
var pitch_ = 0;
var airspeed_ = 50;
/* Keybord Values */


let COMPASS_STARTING_POS = 10
let ROLL_MAX_DEG = 45
let PITCH_MAX_DEG = 20
let KNOTT_MIN = 60
let PER_MINUTE = 0
let UP_AIR = 0
let OVERLOAD_KNOTT = 220
let STOLL_STATE = false


let MAX_FEET = 10000
let MAX_FEET_PRE = 9000
var plcFlag = {
    'read': true,
    'write': true
};
var nrstMapStatus = false;



let currTime, lastTime;
let leftpanelIndicatorDirs = {
    "fuelFlow": 1,
    "oilPress": 1,
    "egt": 1,
    "vac": 1
}


// var engButton = document.getElementById("ENG")
// var clock = document.getElementById("time")
// var mapButton = document.getElementById("NRST")
// var map = document.getElementById("NRST-MAP");
// var backGround = document.getElementById("backGround")
var apArr = document.getElementById("APARROW")
///////////////////////ATTITUDEHERE
var attitudeIndicator = {
    'main': document.getElementById("backGround"),
    'rollPanel': document.getElementById("ROLL_PANEL"),
    'IndicatorDeg': document.getElementById("degree"),
    'roll': 0,
    'pitch': 0,
    'move': function (roll, pitch) {
        attitudeIndicator.roll = roll
        attitudeIndicator.pitch = pitch /// +30 val -30 val

        if (!failController.airsStatus) {
            attitudeIndicator.IndicatorDeg.style.backgroundPositionY = (-483 + attitudeIndicator.pitch * 13) + "px"
            attitudeIndicator.rollPanel.style.transform = 'rotate(' + (-1 * roll) + 'deg)';
            attitudeIndicator.main.style.background = "linear-gradient(" + (180 - roll) + "deg, rgba(5,69,254,1)" + (49 + attitudeIndicator.pitch) + "%, rgba(255,255,255,1) " + (50 + attitudeIndicator.pitch) + "%, rgb(81, 48, 7) " + (50 + attitudeIndicator.pitch) + "%, rgb(81, 48, 7) 100%)";
        }

        if (controlpanel.AP) {
            if (controlpanel.APMASTER) {
                if (controlpanel.ROLL) {
                    if (APcontrol.ROLL > roll_) {

                        apArr.style.transform = "rotate(" + Math.abs(APcontrol.ROLL - roll_) + "deg)"
                        if (Math.abs(APcontrol.ROLL - roll_) > 0.1)
                            roll_ += 0.08
                    } else {
                        apArr.style.transform = "rotate(" + (-1 * Math.abs(APcontrol.ROLL - roll_)) + "deg)"
                        if (Math.abs(APcontrol.ROLL - roll_) > 0.1)
                            roll_ -= 0.08
                    }
                }

                if (controlpanel.PITCH) {
                    apArr.style.top = 417 - ((APcontrol.PITCH - pitch_) * 10.8) + "px"
                    if (APcontrol.PITCH > pitch_) {
                        if (Math.abs(APcontrol.PITCH - pitch_) > 0.1) {
                            pitch_ += 0.08
                            FPM_ += 0.008
                        }

                    } else {
                        if (Math.abs(APcontrol.PITCH - pitch_) > 0.1) {
                            pitch_ -= 0.08
                            FPM_ -= 0.008
                        }
                    }
                }
            } else {
                if (controlpanel.ROLL) {
                    if (APcontrol.ROLL > roll_) {

                        apArr.style.transform = "rotate(" + Math.abs(APcontrol.ROLL - roll_) + "deg)"
                    } else {
                        apArr.style.transform = "rotate(" + (-1 * Math.abs(APcontrol.ROLL - roll_)) + "deg)"
                    }
                }

                if (controlpanel.PITCH) {
                    apArr.style.top = 417 - ((APcontrol.PITCH - pitch_) * 10.8) + "px"
                }
            }
        }
    }
}

var leftpanel = {
    'main': document.getElementById("left-panel"),
    'rpm': document.getElementById("h2"),
    'rpmIdle': document.getElementById("RPM"),
    'fuelflowIdle': document.getElementById("FFLOW"),
    'oilpressIdle': document.getElementById("OIL-PRESS"),
    'oiltempIdle': document.getElementById("OIL-TEMP"),
    'egtIdle': document.getElementById("EGT"),
    'vacIdle': document.getElementById("VAC"),
    'fuelqtyIdle': document.getElementById("FUEL-QTY"),
    'engineHours': document.getElementById("ENG-HRS"),
    'busM': document.getElementById("BUS-M"),
    'busE': document.getElementById("BUS-E"),
    'battM': document.getElementById("BATT-M"),
    'battE': document.getElementById("BATT-E"),
    'panelShow': true,
    'values': {
        'genFuelFlow': 60,
        'genOilPress': 50,
        'genOilTemp': 0,
        'genEgt': 0,
        'genVac': 50,
        'genFuelQty': 75,
        'busM': 24.2,
        'busE': 24.5,
        'battM': -16.4,
        'battE': -5.2,

    },
    'move': function (rpmval, rpmdeg, fuelflowdeg, oilpressdeg, oiltempdeg, egtdeg, vacdeg, fuelqtydeg) {

        leftpanel.rpm.innerHTML = Math.ceil(rpmval)
        leftpanel.rpmIdle.style.transform = 'rotate(' + (rpmdeg * 0.078 - 121) + 'deg)'
        leftpanel.fuelflowIdle.style.backgroundPositionX = fuelflowdeg + '%'
        leftpanel.oilpressIdle.style.backgroundPositionX = oilpressdeg + '%'
        leftpanel.oiltempIdle.style.backgroundPositionX = oiltempdeg + '%'
        leftpanel.egtIdle.style.backgroundPositionX = egtdeg + '%'
        leftpanel.vacIdle.style.backgroundPositionX = vacdeg + '%'
        leftpanel.fuelqtyIdle.style.backgroundPositionX = fuelqtydeg + '%'
    },
    'show': function () {
        leftpanel.main.style.display = '';
        leftpanel.panelShow = true;
    },
    'close': function () {
        leftpanel.main.style.display = 'none';
        leftpanel.panelShow = false;
    },
    "updateValues": function () {
        if (!lastTime) {
            let date = new Date();
            lastTime = date.getTime();
        }
        let date = new Date();
        let currTime = date.getTime();
        let timeDist = currTime - lastTime;

        let {
            genFuelFlow,
            genOilPress,
            genOilTemp,
            genEgt,
            genVac,
            genFuelQty,
            busM,
            busE,
            battM,
            battE
        } = leftpanel.values;

        if (controlpanel.START_STOP) {
            // FuelFlow 
            genFuelFlow = mastercontrol.RPM / 32;
            // OilPress
            genOilPress = mastercontrol.RPM == 0 ? 0 : scaleValue(mastercontrol.RPM, [700, 2900], [40, 80]);
            // OilTemp
            if (mastercontrol.RPM > 0 && genOilTemp < 80) {
                genOilTemp += mastercontrol.RPM / 3500;
            }
            // OilTemp Cooling
            if (mastercontrol.RPM == 0 && genOilTemp > 0 && timeDist % 2000 == 0) {
                genOilTemp -= 1;
            }
            // Egt
            if (mastercontrol.RPM > 0 && genEgt < 70) {
                genEgt += mastercontrol.RPM / 4500;
            }
            // Egt Cooling
            if (mastercontrol.RPM == 0 && genEgt > 0 && timeDist % 300 == 0) {
                genEgt -= 1;
            }
            // Vac
            if (timeDist % 3000 == 0) {
                if (genVac == 60) {
                    leftpanelIndicatorDirs.vac = -1
                } else if (genOilPress == 40) {
                    leftpanelIndicatorDirs.vac = 1
                }
                genVac += leftpanelIndicatorDirs.vac;
            }

            // FuelQty
            if (mastercontrol.RPM != 0) {
                Math.ceil(genFuelQty) != 0 ? genFuelQty -= mastercontrol.RPM * 0.00001 : genFuelQty = 75;
            }

            // EngHrs
            if (timeDist % 60000 == 0) {
                let engineHours = +localStorage.getItem("engineHours");
                localStorage.setItem("engineHours", engineHours + 0.1666);
                let c = Math.ceil(engineHours)
                let s = c >= 10000 ? c : "000" + c;
                leftpanel.engineHours.innerHTML = s.substr(s.length - 4);
            }

            // BUS M
            if (timeDist % 6000 == 0) {
                if (busM < 24) {
                    busM += 0.1;
                } else if (busM > 25) {
                    busM -= 0.1;
                } else {
                    Math.random() >= 0.5 ? busM += 0.1 : busM -= 0.1
                }
                leftpanel.busM.innerHTML = busM.toFixed(1);
            }

            // BUS E
            if (timeDist % 5000 == 0) {
                if (busE < 24) {
                    busE += 0.1;
                } else if (busE > 25) {
                    busE -= 0.1;
                } else {
                    Math.random() >= 0.5 ? busE += 0.1 : busE -= 0.1
                }
                leftpanel.busE.innerHTML = busE.toFixed(1);
            }

            // Batt M
            if (timeDist % 6500 == 0) {
                if (battM < -17) {
                    battM += 0.1;
                } else if (battM > -16) {
                    battM -= 0.1;
                } else {
                    Math.random() >= 0.5 ? battM += 0.1 : battM -= 0.1
                }
                leftpanel.battM.innerHTML = battM.toFixed(1);
            }

            // Batt E
            if (timeDist % 5500 == 0) {
                if (battE < -6) {
                    battE += 0.1;
                } else if (battE > -5) {
                    battE -= 0.1;
                } else {
                    Math.random() >= 0.5 ? battE += 0.1 : battE -= 0.1
                }
                leftpanel.battE.innerHTML = battE.toFixed(1)
            }


        } else {
            // OilTemp Cooling
            if (genOilTemp > 0 && timeDist % 2000 == 0) {
                genOilTemp -= 1;
            }
            // Egt Cooling
            if (genEgt > 0 && timeDist % 300 == 0) {
                genEgt -= 1;
            }

            // BUS M
            if (timeDist % 6000 == 0) {
                if (busM < 24) {
                    busM += 0.1;
                } else if (busM > 25) {
                    busM -= 0.1;
                } else {
                    Math.random() >= 0.5 ? busM += 0.1 : busM -= 0.1
                }
                leftpanel.busM.innerHTML = busM.toFixed(1);
            }

            // BUS E
            if (timeDist % 5000 == 0) {
                if (busE < 24) {
                    busE += 0.1;
                } else if (busE > 25) {
                    busE -= 0.1;
                } else {
                    Math.random() >= 0.5 ? busE += 0.1 : busE -= 0.1
                }
                leftpanel.busE.innerHTML = busE.toFixed(1);
            }

            // Batt M & Batt E
            leftpanel.battM.innerHTML = '0.0';
            leftpanel.battE.innerHTML = '0.0';
        }

        leftpanel.values = {
            genFuelFlow,
            genOilPress,
            genOilTemp,
            genEgt,
            genVac,
            genFuelQty,
            busM,
            busE,
            battM,
            battE
        };

    }
}
let engineHours = Math.ceil(+window.localStorage.getItem("engineHours"))
let s = engineHours >= 10000 ? engineHours : "000" + engineHours;
leftpanel.engineHours.innerHTML = s.substr(s.length - 4);


var failController = {
    'altitudeDom': document.getElementById("altitudeFail"),
    'attitudeDom': document.getElementById("attitudeFail"),
    'airspeedDom': document.getElementById("airspeedFail"),
    'vartspeedDom': document.getElementById("vartspeedFail"),
    'hdgDom': document.getElementById("hdgFail"),
    'vsDom': document.getElementById("VS-FAIL"),
    'adcStatus': false,
    'airsStatus': false,
    'vsStatus': false,
    'pitchStatus': false,
    'rollStatus': false,
    'apStatus': false,
    'oilPressureStatus': false,
    'lowVoltsStatus': false,
    'lowVacuumStatus': false,
    'dgsStatus': false,
    'ahrsStatus': false,
    'crashStatus': false,
    'vsKnottValue': 0,
    'set': function (obj) {

        // Check ADC fail status
        if (obj.hasOwnProperty('apStatus') && this.apStatus != obj.apStatus) {
            obj.apStatus ? (
                document.getElementById('ap-fail-error').style.opacity = 1
            ) : (
                document.getElementById('ap-fail-error').style.opacity = 0.3
            )
        }

        // Check ADC fail status
        if (obj.hasOwnProperty('adcStatus') && this.adcStatus != obj.adcStatus) {
            obj.adcStatus ? (
                document.getElementById('altitudeCounter').style.visibility = 'hidden',
                this.altitudeDom.style.visibility = 'visible',
                document.getElementById('airspeedCounter').style.visibility = 'hidden',
                this.airspeedDom.style.visibility = 'visible',
                this.vartspeedDom.style.visibility = 'visible',
                document.getElementById('adc-fail-error').style.opacity = 1
            ) : (
                document.getElementById('altitudeCounter').style.visibility = 'visible',
                this.altitudeDom.style.visibility = 'hidden',
                document.getElementById('airspeedCounter').style.visibility = 'visible',
                this.airspeedDom.style.visibility = 'hidden',
                this.vartspeedDom.style.visibility = 'hidden',
                document.getElementById('adc-fail-error').style.opacity = 0.3
            )
        }

        // Check AIRS fail status
        if (obj.hasOwnProperty('airsStatus') && this.airsStatus != obj.airsStatus) {
            obj.airsStatus ? (
                this.attitudeDom.style.visibility = 'visible',
                this.hdgDom.style.visibility = 'visible',
                document.getElementById('airs-fail-error').style.opacity = 1
            ) : (
                this.attitudeDom.style.visibility = 'hidden',
                this.hdgDom.style.visibility = 'hidden',
                document.getElementById('airs-fail-error').style.opacity = 0.3
            )
        }
        // Check AHRS fail status
        if (obj.hasOwnProperty('ahrsStatus') && this.ahrsStatus != obj.ahrsStatus) {
            obj.ahrsStatus ? (
                this.attitudeDom.style.visibility = 'visible',
                this.hdgDom.style.visibility = 'visible',
                document.getElementById('airs-fail-error').style.opacity = 1
            ) : (
                this.attitudeDom.style.visibility = 'hidden',
                this.hdgDom.style.visibility = 'hidden',
                document.getElementById('airs-fail-error').style.opacity = 0.3
            )
        }
        // Check vs fail status
        if (obj.hasOwnProperty('vsStatus')) {
            if (obj.vsStatus) {
                if (this.vsKnottValue <= 10) {
                    this.vsDom.style.visibility = 'visible'
                    //////////////////////////////////don't forget to reset the vsKnottValue where READ////////////////////////////////
                    this.vsKnottValue += 0.01
                    var vs_hdg = document.getElementById("span-vs-1")
                    vs_hdg.innerHTML = parseInt(Math.abs((360 + compas.compassCoef + mastercontrol.PEDDAL - 90) % 360))
                    var vsKnottValue_fail = document.getElementById("span-vs-2")
                    vsKnottValue_fail.innerHTML = parseInt(failController.vsKnottValue) + " KT"
                }
            } else {
                this.vsDom.style.visibility = 'hidden'
                this.vsKnottValue = 0

            }
        }

        // Check pitch fail status
        if (obj.hasOwnProperty('pitchStatus') && this.pitchStatus != obj.pitchStatus) {
            obj.pitchStatus ?
                document.getElementById('pitch-error').style.opacity = 1 :
                document.getElementById('pitch-error').style.opacity = 0.3;
        }

        // Check roll fail status
        if (obj.hasOwnProperty('rollStatus') && this.rollStatus != obj.rollStatus) {
            obj.rollStatus ?
                document.getElementById('roll-error').style.opacity = 1 :
                document.getElementById('roll-error').style.opacity = 0.3;
        }

        // Check Oil Pressure fail status
        if (obj.hasOwnProperty('oilPressureStatus') && this.oilPressureStatus != obj.oilPressureStatus) {
            obj.oilPressureStatus ?
                document.getElementById('oil-pressure-error').style.opacity = 1 :
                document.getElementById('oil-pressure-error').style.opacity = 0.3;
        }

        // Check Low Volts fail status
        if (obj.hasOwnProperty('lowVoltsStatus') && this.lowVoltsStatus != obj.lowVoltsStatus) {
            obj.lowVoltsStatus ?
                document.getElementById('low-volts-error').style.opacity = 1 :
                document.getElementById('low-volts-error').style.opacity = 0.3;
        }

        // Check Low Vacuum fail status
        if (obj.hasOwnProperty('lowVacuumStatus') && this.lowVacuumStatus != obj.lowVacuumStatus) {
            obj.lowVacuumStatus ? (
                // this.attitudeDom.style.visibility = 'visible',
                // this.hdgDom.style.visibility = 'visible',
                // document.getElementById('dgs-fail-error').style.opacity = 1,
                document.getElementById('low-vacuum-error').style.opacity = 1
            ) : (
                // this.attitudeDom.style.visibility = 'hidden',
                // this.hdgDom.style.visibility = 'hidden',
                // document.getElementById('dgs-fail-error').style.opacity = 0.3,
                document.getElementById('low-vacuum-error').style.opacity = 0, 3
            )
        }

        // Check WOW status
        if (obj.hasOwnProperty('wowStatus') && this.wowStatus != obj.wowStatus) {
            obj.wowStatus ?
                document.getElementById('wow-status').style.opacity = 1 :
                document.getElementById('wow-status').style.opacity = 0.3;
        }

        // Check DGS fail status
        if (obj.hasOwnProperty('dgsStatus') && this.dgsStatus != obj.dgsStatus && !this.lowVacuumStatus) {
            obj.dgsStatus ? (
                this.attitudeDom.style.visibility = 'visible',
                this.hdgDom.style.visibility = 'visible',
                document.getElementById('dgs-fail-error').style.opacity = 1
            ) : (
                this.attitudeDom.style.visibility = 'hidden',
                this.hdgDom.style.visibility = 'hidden',
                document.getElementById('dgs-fail-error').style.opacity = 0.3
            )
        }


        // Check Crash status
        if (obj.hasOwnProperty('crashStatus') && this.crashStatus != obj.crashStatus) {
            obj.crashStatus ?
                document.getElementById('crash-error').style.opacity = 1 :
                document.getElementById('crash-error').style.opacity = 0.3;
        }

        // Save current settings
        if (obj.hasOwnProperty('adcStatus')) failController.adcStatus = obj.adcStatus;
        if (obj.hasOwnProperty('airsStatus')) failController.airsStatus = obj.airsStatus;
        if (obj.hasOwnProperty('vsStatus')) failController.vsStatus = obj.vsStatus;
        if (obj.hasOwnProperty('pitchStatus')) failController.pitchStatus = obj.pitchStatus;
        if (obj.hasOwnProperty('rollStatus')) failController.rollStatus = obj.rollStatus;
        if (obj.hasOwnProperty('oilPressureStatus')) failController.oilPressureStatus = obj.oilPressureStatus;
        if (obj.hasOwnProperty('lowVoltsStatus')) failController.lowVoltsStatus = obj.lowVoltsStatus;
        if (obj.hasOwnProperty('lowVacuumStatus')) failController.lowVacuumStatus = obj.lowVacuumStatus;
        if (obj.hasOwnProperty('wowStatus')) failController.wowStatus = obj.wowStatus;
        if (obj.hasOwnProperty('dgsStatus')) failController.dgsStatus = obj.dgsStatus;
        if (obj.hasOwnProperty('crashStatus')) failController.crashStatus = obj.crashStatus;
        if (obj.hasOwnProperty('apStatus')) failController.apStatus = obj.apStatus;
        if (obj.hasOwnProperty('ahrsStatus')) failController.ahrsStatus = obj.ahrsStatus;

        // Remove AP Fail Error if there is no error, otherwise add AP Fail only once
        if (!this.adcStatus && !this.airsStatus && !this.pitchStatus && !this.rollStatus && !this.dgsStatus && !this.ahrsStatus) {
            document.getElementById('ap-fail-error').style.opacity = 0.3;
            if (this.apStatus) {
                document.getElementById('ap-fail-error').style.opacity = 1;
            } else {
                document.getElementById('ap-fail-error').style.opacity = 0.3;
            }
        } else {
            document.getElementById('ap-fail-error').style.opacity = 1;
        }


    }
}
var i = 0
var airspeed = {
    'main': document.getElementById("airspeed"),
    'autopilotIdle': document.getElementById("airspeedAP"),
    'column': document.getElementById("a2"),
    'columnII': document.getElementById("a3"),
    'airspeedCoef': 0,
    'Knott': 0,
    'KT': document.getElementById("a1-span"),
    'move': function (maindeg, apdeg) {
        airspeed.airspeedCoef = maindeg
        //i += 1
        airspeed.Knott = (airspeed.airspeedCoef)

        airspeed.KT.innerHTML = (parseInt(airspeed.Knott / 5.06)) + " KT"
        airspeed.main.style.backgroundPosition = "10px " + (-2301 + airspeed.airspeedCoef) + "px"
        airspeed.column.style.backgroundPosition = "29px " + (-1589 + (airspeed.airspeedCoef * 0.6275) - ((airspeed.airspeedCoef * 0.6275) % 31.7415)) + "px"
        airspeed.columnII.style.backgroundPosition = "5px " + (-196 + (airspeed.airspeedCoef * 4.78)) + "px"
        airspeed.autopilotIdle.style.backgroundPosition = "99px " + (-16 + (apdeg)) + "px"
    }
}

///////////////////////////ALTIMETREHERE
var altimetre = {
    'main': document.getElementById("altimetre"),
    'autopilotIdle': document.getElementById("altimetreAP"),
    'altimetreFPM': document.getElementById("altIdle"),
    'altimetreValue': document.getElementById("altimetreTop"),
    'column': document.getElementById("ac2"),
    'columnII': document.getElementById("ac3"),
    'altimetreCoef': sessionStorage.getItem('startedFlag') ? 401 : (0, sessionStorage.setItem('startedFlag', true)),
    'masterChange': false,
    'ALT_FLAG': false,
    'ALT_HALF_FEET': 0,
    'LOW_LEVEL': false,
    'check': true,
    'time_': 0,
    'time_k': 0,
    'apAlt': 0, // AP idle position
    'FPM': document.getElementById("FPM_VALUE").innerHTML,
    'move': function (maindeg, ap) {
        /* CRASH and RELOAD PAGE */
        if (altimetre.altimetreCoef < -0.1) {
            client.writeSingleCoil(19, true)
                .then(function (resp) {}).catch(function () {
                    console.error(arguments)
                })

            setTimeout(() => {
                client.writeSingleCoil(19, false)
                    .then(function (resp) {}).catch(function () {
                        console.error(arguments)
                    })
                remote.getCurrentWindow().reload()
            }, 5000)

            failController.set({
                'crashStatus': true
            })
        }

        if (failController.crashStatus)
            altimetre.altimetreCoef = 0
        else if (controlpanel.WOW) {
            altimetre.altimetreCoef += maindeg
        } else {
            altimetre.altimetreCoef = APcontrol.ALT.VAL * (airspeed.Knott / 5.06) / 58
        }

        if (MAX_FEET_PRE < altimetre.altimetreCoef && pitch_ > 0) {
            pitch_ = pitch_ * (MAX_FEET - altimetre.altimetreCoef) / 1000
            FPM_ = pitch_ / 10
        } else if (MAX_FEET_PRE < altimetre.altimetreCoef && pitch_ < 0) {
            altimetre.altimetreCoef += maindeg;
        }

        var difference = 475
        var maxPitch = 7.5 //10 deggre

        if (controlpanel.APMASTER) {

            if (controlpanel.FLC) {
                var k = .05
                if (Math.abs(controlpanel.FEET - altimetre.altimetreCoef) > difference / 2) {

                    if (controlpanel.FEET - altimetre.altimetreCoef > 0) {

                        if (pitch_ < maxPitch) {
                            FPM_ += (k * ((maxPitch - pitch_) / maxPitch)) / 10
                            pitch_ += (k * ((maxPitch - pitch_) / maxPitch))
                            APcontrol.PITCH = pitch_
                            APcontrol.ALT.COEF = FPM_
                        } else if (pitch_ > maxPitch) {
                            FPM_ -= (k * ((pitch_ - maxPitch))) / 10
                            pitch_ -= (k * ((pitch_ - maxPitch)))
                            APcontrol.PITCH = pitch_
                            APcontrol.ALT.COEF = FPM_
                        }
                        altimetre.apAlt = 0
                    } else {
                        if (FPM_ > 0) {
                            FPM_ -= (FPM_ / pitch_) * 0.1
                            pitch_ -= 0.1
                        } else {
                            if (pitch_ > -maxPitch) {
                                FPM_ -= (0.2 * ((maxPitch + pitch_) / maxPitch)) / 10
                                pitch_ -= (0.2 * ((maxPitch + pitch_) / maxPitch))
                                APcontrol.PITCH = pitch_
                                APcontrol.ALT.COEF = FPM_
                            } else {
                                FPM_ += (FPM_ / pitch_) * 0.1
                                pitch_ += 0.1
                            }
                        }
                        altimetre.apAlt = 475

                    }
                    altimetre.LOW_LEVEL = false
                    APcontrol.ALT.VAL = altimetre.altimetreCoef
                } else {
                    if (!altimetre.LOW_LEVEL) {
                        if (controlpanel.FEET - altimetre.altimetreCoef > 0) {
                            pitch_ = APcontrol.PITCH * Math.abs(controlpanel.FEET - altimetre.altimetreCoef) / (difference / 2)
                            FPM_ = APcontrol.ALT.COEF * Math.abs(controlpanel.FEET - altimetre.altimetreCoef) / (difference / 2)

                            if (difference / 2 > altimetre.apAlt)
                                altimetre.apAlt = difference / 2 - (Math.abs(controlpanel.FEET - altimetre.altimetreCoef))

                            if (Math.abs(controlpanel.FEET - altimetre.altimetreCoef) < 5 && altimetre.altimetreCoef <= controlpanel.FEET) {
                                altimetre.altimetreCoef = controlpanel.FEET + 1
                            }
                        } else {
                            pitch_ = APcontrol.PITCH * Math.abs(controlpanel.FEET - altimetre.altimetreCoef) / (difference / 2)
                            FPM_ = APcontrol.ALT.COEF * Math.abs(controlpanel.FEET - altimetre.altimetreCoef) / (difference / 2)

                            if (difference / 2 < altimetre.apAlt)
                                altimetre.apAlt = difference - (difference / 2 - (Math.abs(controlpanel.FEET - altimetre.altimetreCoef)))
                        }

                        altimetre.time_ += 0.05 / altimetre.time_k
                    } else {
                        console.log("free");
                    }
                }
            }
            ///////////////////////////////////////////////ALT MODE/////////////////////////////////////////////////

            if (controlpanel.ALT && controlpanel.APMASTER) {
                var easing = BezierEasing(.12, .98, .63, 1.7)
                if (altimetre.time_ <= 1) {
                    pitch_ = APcontrol.PITCH - ((APcontrol.PITCH) * easing(altimetre.time_))
                    //apArr.style.top = 417 + APcontrol.PITCH - ((APcontrol.PITCH) * easing(altimetre.time_ + 0.1)) + "px"
                    FPM_ = pitch_ / 10
                } else {
                    pitch_ = 0
                    FPM_ = 0
                }
                altimetre.time_ += 0.05 / 20
            } else if (controlpanel.ALT && !controlpanel.APMASTER) {
                console.log("not control");
                apArr.style.top = 417 - ((APcontrol.PITCH - pitch_) * 10.8) + "px"
            }

            ///////////////////////////////////////////////VS MODE/////////////////////////////////////////////////

            if (controlpanel.VS && controlpanel.APMASTER) {
                var easing = BezierEasing(.5, .02, .56, .99) //cubic-bezier(.5,.02,.56,.99)
                if (pitch_ > APcontrol.VS / 1085 * 10) {
                    if (altimetre.time_ <= 1) {
                        pitch_ = pitch_ - ((Math.abs(APcontrol.VS / 1085 * 10 - pitch_)) * easing(altimetre.time_))
                        FPM_ = pitch_ / 10
                    } else {
                        pitch_ = pitch_
                        FPM_ = pitch_ / 10
                    }
                } else {
                    if (altimetre.time_ <= 1) {
                        pitch_ = pitch_ + ((Math.abs(APcontrol.VS / 1085 * 10 - pitch_)) * easing(altimetre.time_))
                        FPM_ = pitch_ / 10
                    } else {
                        pitch_ = pitch_
                        FPM_ = pitch_ / 10
                    }
                }
                altimetre.time_ += 0.05 / altimetre.time_k

            } else if (controlpanel.VS && !controlpanel.APMASTER) {
                console.log("not control");
                apArr.style.top = 417 - ((APcontrol.PITCH - pitch_) * 10.8) + "px"
            }
        }
        if (!failController.adcStatus) {
            altimetre.altimetreValue.innerHTML = controlpanel.FEET
            altimetre.main.style.backgroundPosition = "0px " + (-10850 + (altimetre.altimetreCoef * 1.008)) + "px"
            altimetre.column.style.backgroundPosition = "26px " + (-4783 + ((altimetre.altimetreCoef - altimetre.altimetreCoef % 100) * 1.008 * 0.478)) + "px"
            altimetre.columnII.style.backgroundPosition = "5px " + (75 + (altimetre.altimetreCoef * 1.008 * 4.1778)) + "px"
            altimetre.autopilotIdle.style.top = (-15 + altimetre.apAlt) + "px"
            ///////////////100 px 1000FPM
            if (altimetre.altimetreCoef >= 0 && 2000 > parseInt((Math.abs(maindeg) * 1.086) * 1000 - (((Math.abs(maindeg) * 1.086) * 1000) % 10)))
                altimetre.altimetreFPM.style.top = 387.8 - (((maindeg) * 1.086) * 100) + "px"
            if (maindeg >= 0)
                altimetre.altimetreFPM.innerHTML = parseInt((Math.abs(maindeg) * 1.086) * 1000 - (((Math.abs(maindeg) * 1.086) * 1000) % 10))
            else
                altimetre.altimetreFPM.innerHTML = "-" + parseInt((Math.abs(maindeg) * 1.086) * 1000 - (((Math.abs(maindeg) * 1.086) * 1000) % 10))
        }

    }
}


//////////////////////////COMPASHERE
var compas = {
    'main': document.getElementById("compass"),
    'autopilotIdle': document.getElementById("compassAP"),
    'arrow': document.getElementById("compass-arrow"),
    'compassdeg': document.getElementById("compassdeg"),
    'CRS': document.getElementById("CRS-H3"),
    'HDG': document.getElementById("HDG-H3"),
    'compassCoef': 0,
    'setCompassCoef': function (maindeg, arrowdeg, apdeg) {
        compas.compassCoef = maindeg
        compas.compassdeg.innerHTML = parseInt(Math.abs((360 + compas.compassCoef + parseInt(mastercontrol.PEDDAL) - failController.vsKnottValue + mastercontrol.SLAVE_FREE) % 360))
        compas.main.style.transform = 'rotate(' + (-1 * (compas.compassCoef + parseInt(mastercontrol.PEDDAL) - failController.vsKnottValue + mastercontrol.SLAVE_FREE)) + 'deg)'
        if (apdeg) compas.autopilotIdle.style.transform = 'rotate(' + (apdeg) + 'deg)'
        if (arrowdeg) compas.arrow.style.transform = 'rotate(' + (arrowdeg) + 'deg)'
    },
    'move': function (maindeg, arrowdeg, apdeg) {

        if (!failController.airsStatus) {

            if (arrowdeg) {
                if (arrowdeg > 99) {
                    compas.CRS.innerHTML = arrowdeg
                } else if (arrowdeg < 10) {
                    compas.CRS.innerHTML = "00" + arrowdeg
                } else {
                    compas.CRS.innerHTML = "0" + arrowdeg
                }
            }

            if (apdeg) {
                if (apdeg > 99) {
                    compas.HDG.innerHTML = apdeg
                } else if (apdeg < 10) {
                    compas.HDG.innerHTML = "00" + apdeg
                } else {
                    compas.HDG.innerHTML = "0" + apdeg
                }
            }

            if (altimetre.altimetreCoef == 0) {
                if (airspeed.Knott > 5) {
                    compas.compassCoef += (mastercontrol.PEDDAL * airspeed.Knott) / 1500
                    compas.compassdeg.innerHTML = parseInt(Math.abs((360 + compas.compassCoef) % 360))
                    compas.main.style.transform = 'rotate(' + (-1 * (compas.compassCoef)) + 'deg)'
                }
            } else {

                compas.compassCoef += maindeg / 100
                if (!controlpanel.YD_MODE) {
                    if (mastercontrol.SLAVE_FREE == 0) {
                        compas.compassdeg.innerHTML = parseInt(Math.abs((360 + compas.compassCoef + parseInt(mastercontrol.PEDDAL) - failController.vsKnottValue) % 360))
                        compas.main.style.transform = 'rotate(' + (-1 * (compas.compassCoef + parseInt(mastercontrol.PEDDAL) - failController.vsKnottValue)) + 'deg)'
                    } else {
                        compas.compassdeg.innerHTML = parseInt(Math.abs((360 + compas.compassCoef + parseInt(mastercontrol.PEDDAL) - failController.vsKnottValue + (mastercontrol.SLAVE_FREE - 5)) % 360))
                        compas.main.style.transform = 'rotate(' + (-1 * (compas.compassCoef + parseInt(mastercontrol.PEDDAL) - failController.vsKnottValue + (mastercontrol.SLAVE_FREE - 5))) + 'deg)'
                    }
                } else {
                    if (mastercontrol.SLAVE_FREE == 0) {
                        compas.compassdeg.innerHTML = parseInt(Math.abs((360 + compas.compassCoef + parseInt(mastercontrol.PEDDAL)) % 360))
                        compas.main.style.transform = 'rotate(' + (-1 * (compas.compassCoef + parseInt(mastercontrol.PEDDAL))) + 'deg)'
                    } else {
                        compas.compassdeg.innerHTML = parseInt(Math.abs((360 + compas.compassCoef + parseInt(mastercontrol.PEDDAL + (mastercontrol.SLAVE_FREE - 5))) % 360))
                        compas.main.style.transform = 'rotate(' + (-1 * (compas.compassCoef + parseInt(mastercontrol.PEDDAL) + (mastercontrol.SLAVE_FREE - 5))) + 'deg)'
                    }
                }
                if (apdeg) compas.autopilotIdle.style.transform = 'rotate(' + (apdeg) + 'deg)'
                if (arrowdeg) compas.arrow.style.transform = 'rotate(' + (arrowdeg) + 'deg)'
            }

            if (controlpanel.HDG && controlpanel.APMASTER) {
                var diff = compasDegCalc(controlpanel.HDG_VAL, compas.compassCoef)
                if (diff <= 0) { //cw
                    if (Math.abs(diff) < 5) {
                        roll_ = -APcontrol.ROLL * diff / 5
                    } else {
                        if (roll_ > -15)
                            roll_ -= 0.1
                        APcontrol.ROLL = roll_
                    }
                } else { //ccw
                    if (Math.abs(diff) < 5) {
                        roll_ = APcontrol.ROLL * diff / 5
                    } else {
                        if (roll_ < 15)
                            roll_ += 0.1
                        APcontrol.ROLL = roll_
                    }
                }
            }
        }


    }
}

///////////////////////////controlpanelhere

var controlpanel = {
    'AP': 0,
    'FD': 0,
    'ALT': 0,
    'FLC': 0,
    'VS': 0,
    'SPEED': 0,
    'ROLL': 0,
    'PITCH': 0,
    'HDG': 0,
    'NEAR': 0,
    'PTICH_TRIM': {
        'UP': 0,
        'DOWN': 0
    },
    'YD_MODE': 0,
    //////////////////////////
    'FEET': 0,
    'IAS': 0,
    'HDG_VAL': 0,
    'FPM': 0,
    //////////////////////////
    'START_STOP': 0,
    'APMASTER': 0,
    'VACUUM': 0,
    'WOW': 0,
    'MOTOR_STATUS': 0,
    'READ_VALUE': function (arrayList) {
        controlpanel.AP = arrayList[0]
        controlpanel.FD = arrayList[1]
        controlpanel.ALT = arrayList[2]
        controlpanel.FLC = arrayList[3]
        controlpanel.VS = arrayList[4]
        controlpanel.SPEED = arrayList[5]
        controlpanel.ROLL = arrayList[6]
        controlpanel.PITCH = arrayList[7]
        controlpanel.HDG = arrayList[8]
        controlpanel.NEAR = arrayList[9]
        controlpanel.PTICH_TRIM.UP = arrayList[10]
        controlpanel.PTICH_TRIM.DOWN = arrayList[11]
        controlpanel.YD_MODE = arrayList[12]
        controlpanel.START_STOP = arrayList[13]
        controlpanel.APMASTER = arrayList[14]
        controlpanel.VACUUM = arrayList[15]
    },
    'STATE_CONTROL': function (list) {
        if (Math.abs(controlpanel.AP - list[0]) != 0) {
            APcontrol.ROLL = roll_
            APcontrol.PITCH = pitch_
            apArr.style.opacity = +controlpanel.FD
            altimetre.time_ = 0
        }
        if (Math.abs(controlpanel.ROLL - list[6]) != 0) {
            APcontrol.ROLL = roll_
        }
        if (Math.abs(controlpanel.PITCH - list[7]) != 0) {
            APcontrol.PITCH = pitch_
        }
        if (Math.abs(controlpanel.FLC - list[3]) != 0) {
            if (Math.abs(controlpanel.FEET - altimetre.altimetreCoef) < 237) {
                altimetre.LOW_LEVEL = true
            } else
                altimetre.LOW_LEVEL = false
            altimetre.time_ = 0
            APcontrol.PITCH = pitch_
            APcontrol.ALT.COEF = pitch_ / 10
        }


        if (Math.abs(controlpanel.ALT - list[2]) != 0) {
            altimetre.time_ = 0
            APcontrol.PITCH = pitch_
            APcontrol.ALT.VAL = altimetre.altimetreCoef
            APcontrol.ALT.COEF = pitch_ / 10
        }

        if (Math.abs(controlpanel.VS - list[4]) != 0) {
            altimetre.time_ = 0
            APcontrol.PITCH = pitch_
            APcontrol.ALT.VAL = altimetre.altimetreCoef
            APcontrol.ALT.COEF = pitch_ / 10
        }
        if (Math.abs(controlpanel.HDG - list[8]) != 0) {
            controlpanel.ROLL = false
            controlpanel.HDG = !controlpanel.HDG
            APcontrol.ROLL = roll_ + 1
            if (mastercontrol.SLAVE_FREE != 0)
                APcontrol.HDG = compas.compassCoef + mastercontrol.PEDDAL + (mastercontrol.SLAVE_FREE - 5) - failController.vsKnottValue
            else
                APcontrol.HDG = compas.compassCoef + mastercontrol.PEDDAL - failController.vsKnottValue
        }
        if (controlpanel.NEAR) {
            if (altimetre.altimetreCoef >= 500) {
                var map = document.getElementById("NRST-MAP");
                map.style.visibility = 'visible';
                nrstMapStatus = true;
                activateMap()
            }
            controlpanel.NEAR = false
        }

        if (Math.abs(controlpanel.APMASTER - list[14]) != 0) {
            if (controlpanel.AP && !controlpanel.APMASTER) {
                altimetre.time_ = 0

                if (controlpanel.ALT) {
                    altimetre.time_ = 0
                    APcontrol.PITCH = pitch_
                    APcontrol.ALT.VAL = altimetre.altimetreCoef
                    APcontrol.ALT.COEF = pitch_ / 10
                }
                if (controlpanel.FLC) {
                    Math.abs(controlpanel.FEET - altimetre.altimetreCoef) < 237 ? altimetre.LOW_LEVEL = true : altimetre.LOW_LEVEL = false
                }
            }
        }
        if (controlpanel.WOW) {

        }
        if (Math.abs(controlpanel.PTICH_TRIM.UP - list[10]) != 0) {
            if (controlpanel.AP) {
                APcontrol.ALT.VAL = altimetre.altimetreCoef
            }
            // AP Disable
        }
        if (Math.abs(controlpanel.PTICH_TRIM.DOWN - list[11]) != 0) {
            if (controlpanel.AP) {
                // Animation AP Disable
            }
            // AP Disable
        }

        if (this.START_STOP) {
            if (Math.abs(controlpanel.MOTOR_STATUS - !list[16]) != 0) {
                controlpanel.MOTOR_STATUS = !list[16];
                controlpanel.MOTOR_STATUS ? (
                    failController.set({
                        'apStatus': false,
                        'lowVoltStatus': false,
                        'oilPressStatus': false,
                        'lowVacuum': false
                    })) : (
                    failController.set({
                        'apStatus': true,
                        'lowVoltStatus': true,
                        'oilPressStatus': true,
                        'lowVacuum': true
                    })
                )
            }
        }

        if (this.START_STOP && controlpanel.MOTOR_STATUS) {
            if (controlpanel.VACUUM == 1) {
                failController.set({
                    'lowVacuumStatus': false
                })
            } else {
                failController.set({
                    'lowVacuumStatus': true
                })
            }
        }

        if (!controlpanel.APMASTER) {
            document.getElementById("AP").style.color = 'Yellow'
        } else {
            document.getElementById("AP").style.color = '#24DA25'
        }
    }
}

var mastercontrol = {
    'YOKE': {
        'ROLL': 0,
        'PITCH': 0
    },
    'THROTTLE': 0,
    'MIXTURE': 0,
    'PEDDAL': 0,
    'RPM': 0,
    'SLAVE_FREE': 0
}

var APcontrol = {
    'ROLL': 0,
    'PITCH': 0,
    'KNOTT': 0,
    'HDG': 0,
    'VS': 0,
    'ALT': {
        'VAL': 0,
        'COEF': 0
    }
}

// start stop
function START_STOP(SS_STATE) {
    if (SS_STATE != 1 || !controlpanel.MOTOR_STATUS) {
        failController.set({
            'apStatus': true,
            'oilPressureStatus': true,
            'lowVoltsStatus': true,
            'lowVacuumStatus': true
        })
    } else {
        failController.set({
            'apStatus': false,
            'oilPressureStatus': false,
            'lowVoltsStatus': false
        })
    }
    if (controlpanel.START_STOP == 0) {
        var RPM = document.getElementById('RPM');
        RPM.style.transform = "rotate(-121deg)"
        var RPM = document.getElementById('RPM');
        RPM.classList.remove("active");
    }
}

var HDG_VAL = 0;
var FEET_VAL = 0;
var IAS_VAL = 0;

var compdeg = 0;
var gradiantdeg = 0;
var gradiantdeg2 = 0;
var airspeedValue = 0;

var connecFlag = false,
    isPLCReadOK = false;





socket.on('connect', function (e) {
    console.log('ON CONNECT')
    connecFlag = true
})

var writeSw = [0, 0]
var isChangePitch = [0, 0]
var isChange = [0, 0]
var lastTransfered = {
    'pitch': null,
    'roll': null
}

var pitchbuffer = new ArrayBuffer(4);
var pitchView = new Uint16Array(pitchbuffer);
var floatView = new Float32Array(pitchbuffer);

function float32ToUint16(p) {


    floatView[0] = p
    //floatView2[0] = r
    //floatView3[0] = att
    return [pitchView[0], pitchView[1]];
}

function uint16ToFloat32(low, high) {
    var buffer = new ArrayBuffer(4);
    var intView = new Uint16Array(buffer);
    var floatView = new Float32Array(buffer);

    intView[0] = low;
    intView[1] = high;
    return floatView[0];
}


function socketRead() {
    document.getElementById("RemoteControl").style.visibility = 'hidden';
    document.getElementById("ConnectionState").style.visibility = 'hidden'


    client.readHoldingRegisters(2100, 6)
        .then((resp) => {
            if (plcFlag.read) {
                let _low = resp.response._body._values[0]
                let _high = resp.response._body._values[1]


                mastercontrol.YOKE.PITCH = uint16ToFloat32(_low, _high) //-10 to 10

                _low = resp.response._body._values[2]
                _high = resp.response._body._values[3]


                mastercontrol.YOKE.ROLL = uint16ToFloat32(_low, _high) //-10 to 10

                _low = resp.response._body._values[4]
                _high = resp.response._body._values[5]

                APcontrol.VS = uint16ToFloat32(_low, _high) //-10 to 10

                ///////////////////NOT TESTED
                if (Math.abs(controlpanel.FPM - APcontrol.VS) != 0 && !controlpanel.ALT) {
                    altimetre.time_k = ((Math.abs(controlpanel.FPM - (FPM_ * 1086))) / 8.5) // 450
                    altimetre.time_ = 0
                    controlpanel.FPM = APcontrol.VS
                }
                if (!controlpanel.AP || !controlpanel.APMASTER) {
                    // console.log(pitch_);
                    // console.log(roll_);

                    pitch_ = mastercontrol.YOKE.PITCH // scale               
                    roll_ = mastercontrol.YOKE.ROLL
                    FPM_ = pitch_ / 10
                }
            }
        }).catch(function () {
            console.error(arguments)
        })

    client.readCoils(0, 17)
        .then(function (resp) {
            if (plcFlag.read) {
                //console.log(resp.response._body._valuesAsArray)
                controlpanel.STATE_CONTROL(resp.response._body._valuesAsArray)
                controlpanel.READ_VALUE(resp.response._body._valuesAsArray)
            }
        }).catch(function () {
            console.error(arguments)
        })

    client.readCoils(18, 1)
        .then(function (resp) {
            if (plcFlag.read) {
                controlpanel.WOW = resp.response._body._valuesAsArray[0]
                failController.set({
                    'wowStatus': !controlpanel.WOW
                })
            }
        }).catch(function () {
            console.error(arguments)
        })
    ///////////////////////////READFAIL
    client.readCoils(21, 8)
        .then(function (resp) {
            if (plcFlag.read) {
                console.log(resp.response._body._valuesAsArray);
                failController.set({
                    'adcStatus': resp.response._body._valuesAsArray[0],
                    'airsStatus': resp.response._body._valuesAsArray[1],
                    'pitchStatus': resp.response._body._valuesAsArray[3],
                    'rollStatus': resp.response._body._valuesAsArray[4],
                    'apStatus': resp.response._body._valuesAsArray[5],
                    'dgsStatus': resp.response._body._valuesAsArray[6],
                    'ahrsStatus': resp.response._body._valuesAsArray[7]
                })
            }
        }).catch(function () {
            console.error(arguments)
        })

    client.readHoldingRegisters(100, 8)
        .then(function (resp) {
            if (plcFlag.read) {
                // control feet isChange
                if (Math.abs(controlpanel.FEET - resp.response._body._values[0]) != 0) {
                    if (Math.abs(altimetre.altimetreCoef - resp.response._body._values[0]) < 275)
                        altimetre.LOW_LEVEL = true
                }
                controlpanel.FEET = resp.response._body._values[0]
                controlpanel.IAS = resp.response._body._values[1]
                controlpanel.HDG_VAL = resp.response._body._values[2]
                mastercontrol.RPM = resp.response._body._values[3]
                console.log('RPM => ', mastercontrol.RPM)
                mastercontrol.PEDDAL = (resp.response._body._values[4] - 500) / 50 //-15 to 15
                mastercontrol.THROTTLE = resp.response._body._values[5]
                failController.vsKnottValue = resp.response._body._values[6] / 10
                mastercontrol.SLAVE_FREE = resp.response._body._values[7]
                failController.set({
                    'vsStatus': failController.vsKnottValue
                })
                isPLCReadOK = true;
            }
        }).catch(function () {
            console.error(arguments)
        })
}


function socketWrite() {
    if (nrstMapStatus) {
        controlpanel.AP = 1
        controlpanel.APMASTER = 1
    }

    if (controlpanel.AP && controlpanel.APMASTER && plcFlag.write) {
        if (lastTransfered.pitch == null || lastTransfered.pitch.toFixed(2) != pitch_.toFixed(2) || nrstMapStatus) {
            lastTransfered.pitch = pitch_;
            client.writeMultipleRegisters(2200, float32ToUint16(pitch_.toFixed(2)))
                .then(function (resp) {
                    // console.log(resp);
                }).catch(function () {
                    console.error(arguments)
                })
        }
        if (lastTransfered.roll == null || lastTransfered.roll.toFixed(2) != roll_.toFixed(2) || nrstMapStatus) {
            lastTransfered.roll = roll_;
            client.writeMultipleRegisters(2202, float32ToUint16(roll_.toFixed(2)))
                .then(function (resp) {
                    // console.log(resp);
                }).catch(function () {
                    console.error(arguments)
                })
        }
    }
    if (altimetre.altimetreCoef > 500) {
        if (writeSw[0] == 0) {
            client.writeSingleCoil(20, true)
                .then(function (resp) {
                    // console.log(resp);
                }).catch(function () {
                    console.error(arguments)
                })
        }
        writeSw[0] = 1
        writeSw[1] = 0
    } else {
        if (writeSw[1] == 0) {
            client.writeSingleCoil(20, false)
                .then(function (resp) {
                    // console.log(resp);
                }).catch(function () {
                    console.error(arguments)
                })
        }
        writeSw[0] = 0
        writeSw[1] = 1
    }

    /////////////////////////////////////NOT TESTED
    if (controlpanel.AP && !controlpanel.APMASTER) {
        altimetre.time_ = 0
    }
}

setInterval(() => {
    if (connecFlag) {
        if (plcFlag.read) socketRead()
        if (plcFlag.write) socketWrite()
    } else {
        // if (nrstMapStatus == false) mastercontrol.RPM = mastercontrol.THROTTLE * 20;
    }
}, 100)

var requestAP = document.getElementById("APrequest")
var responseAP = document.getElementById("APresponse")
var request = document.getElementById("request")
var response = document.getElementById("response")

var mainInterval = setInterval(function () {
    //controlpanel.APMASTER = true

    if (connecFlag && isPLCReadOK) {


        optimiseValues()
        VALUEISNAN()

        if (controlpanel.START_STOP) {
            let {
                genFuelFlow,
                genOilPress,
                genOilTemp,
                genEgt,
                genVac,
                genFuelQty
            } = leftpanel.values;
            leftpanel.move(mastercontrol.RPM, mastercontrol.RPM, genFuelFlow, genOilPress, genOilTemp, genEgt, genVac, genFuelQty)
            AP.autopilot(controlpanel, APcontrol)
            SS_STATE = 1
            START_STOP(SS_STATE)
        } else {
            leftpanel.move(mastercontrol.RPM, mastercontrol.RPM, 0, 0, 0, 0, 0, 0)
            SS_STATE = 0
            START_STOP(SS_STATE)
        }
        if (controlpanel.ALT)
            document.getElementById("ALT_VALUE").innerHTML = parseInt(APcontrol.ALT.VAL)
        leftpanel.updateValues();
        airspeed.move(+mastercontrol.THROTTLE, 0)
        attitudeIndicator.move(+roll_, +pitch_);
        altimetre.move(+FPM_, 0);
        compas.move(+roll_, 10, +controlpanel.HDG_VAL)
    }
    startTime()


}, 50);

socket.setTimeout(90000)

socket.connect(options)
socket.on('close', function (e) {
    socket.end()
    connecFlag = false
    console.log('ON CLOSE')
    setTimeout(function () {
        socket.connect(options);
    }, 1000)
});

// socket.on('error', function (e) {
//     connecFlag = false
//     console.log('ON ERROR')
//     //document.getElementById("RemoteControl").style.visibility = 'visible'
//     var connState = document.getElementById("ConnectionState")
//     connState.innerHTML = "No Connection"
//     connState.style.zIndex = 99;
//     connState.style.color = "Red"
//     setTimeout(function () {
//         socket.connect(options);
//     }, 1000)
// })

[controlpanel, mastercontrol] = controlModule.virtualControl(controlpanel, mastercontrol, APcontrol)

function VALUEISNAN() {
    isNaN(FPM_) ? FPM_ = +localStorage.getItem("FPM") : localStorage.setItem("FPM", FPM_);
    isNaN(pitch_) ? pitch_ = +localStorage.getItem("PITCH") : localStorage.setItem("PITCH", pitch_);
    isNaN(roll_) ? roll_ = +localStorage.getItem("ROLL") : localStorage.setItem("ROLL", roll_);
    isNaN(altimetre.altimetreCoef) ? (altimetre.altimetreCoef = 0, altimetre.altimetreCoef = +localStorage.getItem("ATT")) : localStorage.setItem("ATT", altimetre.altimetreCoef);
}

function startTime() {
    let today = new Date();
    let h = today.getHours();
    let m = today.getMinutes();
    let s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('time').innerHTML = "LCL    " +
        h + ":" + m + ":" + s;
    setTimeout(startTime, 1000);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i
    }; // add zero in front of numbers < 10
    return i;
}

function optimiseValues() {
    roll_ = +roll_.toFixed(2);
    pitch_ = +pitch_.toFixed(2);
    FPM_ = +FPM_.toFixed(2);
    //altimetre.altimetreCoef = +altimetre.altimetreCoef.toFixed(2)
    if (APcontrol.ALT.VAL != undefined) APcontrol.ALT.VAL = +APcontrol.ALT.VAL.toFixed(2);
}

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('time').innerHTML = "LCL    " +
        h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i
    }; // add zero in front of numbers < 10
    return i;
}


function compasDegCalc(T, C) {
    return (T - C + 540) % 360 - 180
}