module.exports = {
    'remoteControl' : document.getElementById("RemoteControl"),
    virtualControl:function(controlpanel , mastercontrol , APcontrol)
    {
        /////////////////////////////////////////////////
        controlpanel.FEET         = $(".slider")[0].value;
        controlpanel.IAS          = $(".slider")[1].value;
        controlpanel.HDG_VAL      = $(".slider")[2].value;
        controlpanel.FPM          = $(".slider")[3].value;
        /////////////////////////////////////////////////
        mastercontrol.THROTTLE    = $(".slider")[4].value;
        mastercontrol.MIXTURE     = $(".slider")[5].value;
        mastercontrol.YOKE.ROLL   = $(".slider")[6].value;
        mastercontrol.YOKE.PITCH  = $(".slider")[7].value;
        mastercontrol.PEDDAL      = $(".slider")[8].value;

        $(".remoteCnt").click((e)=>{
            if(e.target.innerText == "AP"){
                controlpanel.AP    = !controlpanel.AP
                controlpanel.ROLL  = true
                controlpanel.PITCH = true
                controlpanel.FD    = true
                controlpanel.APMASTER = true
                APcontrol.ROLL     = roll_
                APcontrol.PITCH    = pitch_                
            }

            else if(e.target.innerText == "FD")
            {
                controlpanel.FD = !controlpanel.FD
                var ap = document.getElementById("APARROW")
                ap.style.opacity = +controlpanel.FD
            }
            

            else if(e.target.innerText == "ALT"){
                altimetre.time_ = 0
                controlpanel.ALT = !controlpanel.ALT
                controlpanel.PITCH = false
                controlpanel.VS    = false
                controlpanel.FLC   = false
                APcontrol.PITCH = pitch_
                APcontrol.ROLL = 0
                APcontrol.ALT.VAL = altimetre.altimetreCoef
                APcontrol.ALT.COEF = pitch_ / 10


            }
            

            else if(e.target.innerText == "FLC")
            {
                Math.abs (controlpanel.FEET - altimetre.altimetreCoef) < 237 ? altimetre.LOW_LEVEL = true : altimetre.LOW_LEVEL = false
                controlpanel.FLC = !controlpanel.FLC
                controlpanel.PITCH = false
                APcontrol.PITCH = pitch_
                APcontrol.ALT.COEF = FPM_
                controlpanel.PITCH  = 0
                controlpanel.ALT    = 0
                controlpanel.VS     = 0
            }
            else if(e.target.innerText == "VS")
            {
                altimetre.time_ = 0
                controlpanel.VS = !controlpanel.VS
                console.log("VS");
                console.log(controlpanel.VS);
                APcontrol.PITCH = pitch_
                APcontrol.ALT.COEF = FPM_
                altimetre.time_k = (1 + (Math.abs(controlpanel.FPM - altimetre.altimetreCoef))/100)*20
                controlpanel.PITCH  = 0
                controlpanel.ALT    = 0
                controlpanel.FLC    = 0
                APcontrol.VS = controlpanel.FPM
            }

            else if(e.target.innerText == "SPEED")
            controlpanel.SPEED = !controlpanel.SPEED

            else if(e.target.innerText == "ROLL")
            {
                controlpanel.ROLL = !controlpanel.ROLL
                controlpanel.HDG = false
            }
            else if(e.target.innerText == "PITCH")
            {
                controlpanel.PITCH = !controlpanel.PITCH
                APcontrol.PITCH     = pitch_

                controlpanel.HDG    = false
                controlpanel.VS     = false
                controlpanel.FLC    = false
                controlpanel.ALT    = false
            }
            

            else if(e.target.innerText == "HDG"){
                controlpanel.ROLL = false
                controlpanel.HDG = !controlpanel.HDG
                APcontrol.ROLL = roll_ + 1
                APcontrol.HDG = compas.compassCoef + mastercontrol.PEDDAL
            }

            else if(e.target.innerText == "NEAR")
            controlpanel.NEAR = !controlpanel.NEAR

            else if(e.target.innerText == "UP")
            {
                controlpanel.UP = !controlpanel.UP
            }
            

            else if(e.target.innerText == "DOWN")
            {
                controlpanel.DOWN = !controlpanel.DOWN
            }
            

            else if(e.target.innerText == "YD MODE")
            controlpanel.YD_MODE = !controlpanel.YD_MODE

            else if(e.target.innerText == "START-STOP"){
                controlpanel.START_STOP = !controlpanel.START_STOP
            }
            

            else{
                console.log("else");
            }
            if(controlpanel.AP == false)
            {
                controlpanel.ROLL  = false
                controlpanel.PITCH = false
                controlpanel.ALT   = false
                controlpanel.HDG   = false
                controlpanel.FLC   = false
                controlpanel.VS    = false
                controlpanel.FD    = false
            }
        })
        $(".slider").change((e)=>{
            /////////////////////////////////////////////////
            controlpanel.FEET         = $(".slider")[0].value;
            controlpanel.IAS          = $(".slider")[1].value;
            controlpanel.HDG_VAL      = $(".slider")[2].value;
            controlpanel.FPM          = $(".slider")[3].value;

            if(Math.abs(controlpanel.FPM - APcontrol.VS) != 0)
            {
                altimetre.time_ = 0
                APcontrol.VS = controlpanel.FPM
                altimetre.time_k = (1 + (Math.abs(controlpanel.FPM - altimetre.altimetreCoef))/100)*20
            }
                
            /////////////////////////////////////////////////
            mastercontrol.THROTTLE    = $(".slider")[4].value;
            mastercontrol.MIXTURE     = $(".slider")[5].value;
            mastercontrol.YOKE.ROLL   = $(".slider")[6].value;
            mastercontrol.YOKE.PITCH  = $(".slider")[7].value;
            mastercontrol.PEDDAL      = $(".slider")[8].value;
        })
        return [controlpanel , mastercontrol]
    },
    show:function()
    {
        this.RemoteControl.style.opacity = 1
    },
    hide:function()
    {
        this.RemoteControl.style.opacity = 0
    }
}

document.addEventListener('keydown', function(event) {
    //console.log(event.keyCode);
    if (event.keyCode == 37) {
        roll_ -= 0.2
        attitudeIndicator.move(roll_, pitch_)

        //alert('Left was pressed');
    } else if (event.keyCode == 38) {
        if (airspeed.Knott > 60) {
            FPM_ += 0.02
            pitch_ += 0.2
            controlpanel.APMASTER = false
                //attitudeIndicator.move(roll_ , pitch_)
        } else
            attitudeIndicator.move(0, 0);
        //alert('UP was pressed');
    } else if (event.keyCode == 39) {
        roll_ += 0.2
        attitudeIndicator.move(roll_, pitch_);
        //alert('Right was pressed');
    } else if (event.keyCode == 40) {
        if (airspeed.Knott > 60) {
            FPM_ -= 0.02
            pitch_ -= 0.2
            controlpanel.APMASTER = false
                //attitudeIndicator.move(roll_ , pitch_);
        } else
            attitudeIndicator.move(0, 0);
        //alert('Down was pressed');
    } else if (event.keyCode == 87) {
        airspeed_ = 0.5
        airspeed.move(airspeed_, 0)
            //alert('W was pressed');
    } else if (event.keyCode == 83) {
        airspeed_ = -0.5
        airspeed.move(airspeed_, 0)
            //alert('S was pressed');
    }
});

var engButton = document.getElementById("ENG")
var mapButton = document.getElementById("NRST")
var map = document.getElementById("NRST-MAP");
var reload    = document.getElementById("RELOAD")

engButton.onclick = function() {
    if (leftpanel.panelShow == true) {
        leftpanel.close();
    } else {
        leftpanel.show();
    }
}

reload.onclick = function() {
    reload.style.transition = "all 2.5s ease-in-out"
    reload.style.transform = "rotate(720deg)"
    localStorage.setItem("ATT", altimetre.altimetreCoef)
    socketWriteNrstStatus(false);
    setTimeout(()=>{
        remote.getCurrentWindow().reload()
    }, 2500)
    
}


var nrstMapStatus = false;
mapButton.onclick = function() {
    if (altimetre.altimetreCoef >= 500 && nrstMapStatus == false && controlpanel.AP) {

        map.style.visibility = 'visible';
        nrstMapStatus = true;
        activateMap()
    }
}