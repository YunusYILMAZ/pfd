// search the CSSOM for a specific -webkit-keyframe rule
function findKeyframesRule(rule) {
    // gather all stylesheets into an array
    var ss = document.styleSheets;

    // loop through the stylesheets
    for (var i = 0; i < ss.length; ++i) {

        // loop through all the rules
        for (var j = 0; j < ss[i].cssRules.length; ++j) {

            // find the -webkit-keyframe rule whose name matches our passed over parameter and return that rule
            if (ss[i].cssRules[j].type == window.CSSRule.KEYFRAMES_RULE && ss[i].cssRules[j].name == rule)
                return ss[i].cssRules[j];
        }
    }

    // rule not found
    return null;
}

// remove old keyframes and add new ones
function change(id, animationName, rules) {
    // find our -webkit-keyframe rule
    var keyframes = findKeyframesRule(animationName);

    // remove the existing rules
    while (keyframes.cssRules[0]) {
        keyframes.deleteRule(keyframes.cssRules[0].keyText)
    }

    rules.map(rule => { keyframes.appendRule(rule); })


    // assign the animation to our element (which will cause the animation to run)
    document.getElementById(id).style.webkitAnimationName = animationName;
}

function randomFromTo(from, to) {
    return Math.floor(Math.random() * (to - from + 1) + from);
}

// begin the new animation process
function changeAnimation(id, animationName, rules, callback) {
    // remove the old animation from our object
    var element = document.getElementById(id);
    element.style.webkitAnimationName = "none";

    // call the change method, which will update the keyframe animation
    setTimeout(function() { change(id, animationName, rules) }, 0);

    if (callback) {
        element.addEventListener("webkitAnimationEnd", () => { callback(element) });
    }
}