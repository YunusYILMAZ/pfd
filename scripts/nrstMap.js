var mapTimeout;
var turningPoint = 365,
    halfTurningPoint = 445,
    correctionPoint = 520,
    landingInPoint = 660,
    pathEndPoint = 675,
    compassCounter = 0;
var fRoll = 0,
    fAltitude = 0,
    fCompass = 0,
    fKnott = 0;

var g = Snap("#planeSvg");
g.attr({
    viewBox: [0, 100, 600, 400]
});

function socketWriteNrstStatus(status) {
    if (typeof (status) == "boolean") {
        client.writeSingleCoil(19, status)
            .then(function (resp) {
            }).catch(function () {
                console.error(arguments)
            })
    }
}

function getReadyForNrstMapPath(callback) {

    document.getElementById('active-mode-panel').style.visibility = 'visible';
    plcFlag.read = false;
    controlpanel.FEET = 500;
    controlpanel.FLC = 1;
    controlpanel.AP = 1;
    controlpanel.APMASTER = 1;
    apArr.style.visibility = 'hidden';

    var interval = setInterval(() => {
        plcFlag.read = false;
        controlpanel.FEET = 500;
        controlpanel.FLC = 1;
        socketWriteNrstStatus(true)


        if (altimetre.altimetreCoef < 600) {

            controlpanel.FLC = 0;
            controlpanel.AP = 1;
            controlpanel.APMASTER = 1;

            clearInterval(interval);
            // socket.end()
            callback();
        }
    }, 1000)
}

Snap.load("./img/nrstMapFlight.svg", function (f) {

    function getShift(dot) {
        return "t" + (400 - dot.x) + "," + (300 - dot.y);
    }
    var parent = f.select("g"),
        flightPath = f.select("#flightPath"),
        plane = f.select("#plane"),
        top = g.g();
    top.add(parent);
    flightPath.attr({
        display: "none"
    });
    plane = parent.g(plane, plane.clone());
    plane.attr({
        display: "none"
    });
    plane[0].attr({
        stroke: "#fff",
        fill: "#fff",
        strokeWidth: 2
    });
    var flightStroke = parent.path().attr({
        fill: "none",
        stroke: "#D417D5",
        strokeWidth: 2
        // strokeDasharray: "5 3"
    }).insertBefore(plane);
    activateMap = function () {
        document.getElementById("NRST-MAP-CONTAINER").style.visibility = 'visible';
        document.getElementById("mapDialog").style.visibility = 'visible';
        socketWriteNrstStatus(true);

        getReadyForNrstMapPath(() => {


            // socket.connect(options)

            document.getElementById("mapDialog").style.visibility = 'hidden';
            plane.attr({
                display: ""
            });
            var len = Snap.path.getTotalLength(flightPath.attr("d"));


            fAltitude = altimetre.altimetreCoef;
            fCompass = compas.compassCoef;
            fKnott = airspeed.Knott / 5.06;
            var l = 0,
                lastTP = turningPoint,
                lastL = l,
                lStepReducer = l,
                finishFlag = [false, false];


                $(planeSvg).show()
            Snap.animate(0, len / 2, function (p) {

                l += mastercontrol.RPM * 0.000050


                
                var dot = flightPath.getPointAtLength(l);

                flightStroke.attr({
                    // d: flightPath.getSubpath(0, len)
                    d: flightPath
                });


                plane.attr({
                    transform: "t" + [dot.x, dot.y] +
                        "r" + (dot.alpha - 90)
                });


                // if (l - lStepReducer >= 0.5) {
                    lStepReducer = l;


                    if (l - lastL > 15) {
                        socketWrite()
                        lastL = l;
                    }

                    if (l > pathEndPoint && !finishFlag[1]) {
                        l = pathEndPoint
                        finishFlag = [true, false]
                    }

                    if (l >= pathEndPoint && !finishFlag[1]) {
                        finishFlag[0] = false
                        finishFlag[1] = true
                        socketWriteNrstStatus(false)
                        setTimeout(() => {
                            remote.getCurrentWindow().reload()
                        }, 2000)
                    }



                    let degree = Math.abs(dot.alpha.toFixed(2))
                    degree >= 180 && degree <= 270 ? scaleValue(degree, [180, 270], [90, 180]) : scaleValue(degree, [90, 20], [270, 180]);

                    //turningPoint-445-correctionPoint

                    // RPM Control
                    // Decrease rpm value periodically
                    
                    if (mastercontrol.RPM > 2300 && l <= turningPoint) {
                        mastercontrol.RPM -= Math.abs(mastercontrol.RPM - 2300) / 800
                    } else if (mastercontrol.RPM >= 1500 && l > turningPoint && l <= correctionPoint) {
                        mastercontrol.RPM -= Math.abs(mastercontrol.RPM - 1500) / 1000
                    } else if (mastercontrol.RPM >= 1000 && l > correctionPoint && l <= landingInPoint) {
                        mastercontrol.RPM -= Math.abs(mastercontrol.RPM - 800) / 1000
                    } else if (mastercontrol.RPM >= 700 && altimetre.altimetreCoef <= 5 && l > landingInPoint) {
                        mastercontrol.RPM -= mastercontrol.RPM / 300
                    }

                    ////////////////////////AIR SPEED 

                    if (l <= correctionPoint)
                        airspeed.move(+((fKnott - ((fKnott - 85) * Math.abs(l / correctionPoint))) * 5.06).toFixed(2))
                    else if (l > correctionPoint && l <= landingInPoint)
                        airspeed.move(+((85 - (25 * Math.abs((l - correctionPoint) / (landingInPoint - correctionPoint)))) * 5.06).toFixed(2))
                    else if (l <= pathEndPoint && l > landingInPoint) {
                        airspeed.move(+((60 - (60 * Math.abs((l - landingInPoint) / (pathEndPoint - landingInPoint)))) * 5.06).toFixed(2))
                    } else {
                        airspeed.move(0)
                    }


                    // COMPASS
                    // Move compass when plane turning
                    if ((degree > 180 || degree < 90) && degree != 0 && l - lastTP > 1) {
                        //document.getElementById("compass").style.transition = "transform 32s ease-in-out";

                        compas.setCompassCoef(+(fCompass + (178 * Math.abs(((correctionPoint - turningPoint) - (correctionPoint - l)) / (correctionPoint - turningPoint)))).toFixed(2));
                    }

                    // ROLL
                    // Change roll_ value when plane turning
                    if (l > turningPoint && l <= correctionPoint && roll_ < 10) {
                        roll_ += 0.05;
                        fRoll = pitch_
                    } else if (l > correctionPoint) {
                        if (roll_ > 0.1)
                            roll_ -= 0.05;

                        pitch_ = +(fRoll * Math.abs(pathEndPoint - l) / Math.abs(pathEndPoint - correctionPoint)).toFixed(2)

                        FPM_ = pitch_ / 10
                        if (pitch_ >= -0.05) {
                            pitch_ = 0
                            FPM_ = 0
                        }
                    }
                    if (l <= pathEndPoint)
                        altimetre.altimetreCoef = fAltitude * (Math.abs(pathEndPoint - l) / pathEndPoint)


                    // Stop plane degree at zero when animation finished
                    if (dot.x === dot.end.x && dot.y === dot.end.y) {
                        dot.alpha = 0;
                    }

                // }
            });
        });
    };
});