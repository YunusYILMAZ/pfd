module.exports = {
    autopilot:function(controlpanel , APcontrol)
    {
        altimetre.autopilotIdle.style.opacity = '1'
        compas.autopilotIdle.style.opacity    = '1'
        airspeed.autopilotIdle.style.opacity  = '1'

        var _autopilot      = document.getElementById("APARROW")
        _autopilot.style.opacity = '1'
        if(controlpanel.AP == true ){
            document.getElementById("AP").style.opacity  = '1'
            document.getElementById("AP").innerHTML      = 'AP'
            ////////////////////////////////////////////////////////
            if(controlpanel.PITCH == 1){
                document.getElementById("ALT").style.opacity    = '0'
                document.getElementById("FPM").style.opacity    = '0'
                document.getElementById("PITCH").style.opacity  = '1'
                document.getElementById("PITCH").innerHTML      = 'PITCH'
            }
            ////////////////////////////////////////////////////////
            if(controlpanel.ROLL == 1){
                document.getElementById("ROLL").style.opacity   = '1'
                document.getElementById("ROLL").innerHTML       = 'ROLL'
            }
            ////////////////////////////////////////////////////////
            if(controlpanel.HDG == 1)
            {
                document.getElementById("ROLL").style.opacity = '1'
                document.getElementById("ROLL").innerHTML = 'HDG'
            }
            ///////////////////////////////////////////////////////
            if(controlpanel.VS == 1)
            {
                document.getElementById("PITCH").style.opacity        = '1'
                document.getElementById("PITCH").innerHTML            = 'VS'
                document.getElementById("ALT").style.opacity          = '0'
                document.getElementById("FPM").style.opacity          = '1'
                document.getElementById("FPM_VALUE").innerHTML  = parseInt(APcontrol.VS)
                document.getElementById("FPM_STRING").innerHTML = 'FPM'
            }
            ////////////////////////////////////////////////////////
            if(controlpanel.FLC == 1)
            {
                document.getElementById("PITCH").style.opacity = '1'
                document.getElementById("PITCH").innerHTML     = 'FLC'
                document.getElementById("FPM").style.opacity   = '0'
                document.getElementById("ALT").style.opacity   = '1'
                document.getElementById("ALT_VALUE").innerHTML = document.getElementById("altimetreTop").innerText
            }

            if(controlpanel.ALT == 1){
                document.getElementById("PITCH").innerHTML      = 'ALT'
                
                document.getElementById("ALT_STRING").innerHTML = 'ALT'
                document.getElementById("PITCH").style.opacity  = '1'
                document.getElementById("FPM").style.opacity    = '0'
                document.getElementById("ALT").style.opacity    = '1'
            }
            if(controlpanel.SPEED == 1){
                document.getElementById("SPEED_").innerHTML = 'SPEED'
                document.getElementById("SPEED_STRING").innerHTML = 'KT'
                document.getElementById("SPEED_VALUE").innerHTML = controlpanel.IAS
                document.getElementById("SPEED").style.opacity  = '1'
            }
            else{
                document.getElementById("SPEED").style.opacity  = '0'
            }

            if(!controlpanel.ALT && !controlpanel.FLC && !controlpanel.VS && !controlpanel.PITCH && !controlpanel.SPEED)
            {
                document.getElementById("PITCH").style.opacity  = '0'
                document.getElementById("FPM").style.opacity    = '0'
                document.getElementById("ALT").style.opacity    = '0'
            }
            if(!controlpanel.ROLL && !controlpanel.HDG)
            {
                document.getElementById("ROLL").style.opacity  = '0'
            }
        }

        else{
            document.getElementById("ROLL").style.opacity  = '0'
            document.getElementById("PITCH").style.opacity = '0'
            document.getElementById("AP").style.opacity    = '0'
            document.getElementById("FPM").style.opacity   = '0'
            document.getElementById("ALT").style.opacity   = '0'
            document.getElementById("SPEED").style.opacity  = '0'
            _autopilot.style.opacity = '0'
        }
    }
}